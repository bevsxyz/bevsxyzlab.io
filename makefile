##
# Hugo Run
#
# @file
# @version 0.1

dev:
	hugo serve \
		--buildDrafts \
		--disableFastRender

dev-nodraft:
	hugo serve
# end
