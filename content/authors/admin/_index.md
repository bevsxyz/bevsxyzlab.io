---
# Display name
title: Bevan Stanely

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Data Scientist

# Status emoji
status:
  icon: ☕️

# Organizations/Affiliations to show in About widget
organizations:
  - name: Indian Institute of Science
    url: 'https://www.iisc.ac.in/'

# Short bio (displayed in user profile at end of posts)
bio: An aspiring Data Scientist. I enjoy narrating data through storytelling.

# Interests to show in About widget
#interests:
#  - Complex Systems
#  - Parallel Programming
#  - Mathematics
#  - Open Source

# Education to show in About widget
#education:
#  courses:
#  - course: Masters in Microbiology and Cell Biology
#    institution: Indian Institute of Science
#    year: 2022
#  - course: BSc in Biological Sciences
#    institution: Bangalore University
#    year: 2019

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: '/contact'
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/bevsxyz
  - icon: github
    icon_pack: fab
    link: https://github.com/bevsxyz
  - icon: youtube
    icon_pack: fab
    link: https://www.youtube.com/channel/UCvpB3HFINFVoUeblgoqbmxw
  - icon: linkedin
    icon_pack: fab
    link: https://in.linkedin.com/in/bevsxyz/
  - icon: instagram
    icon_pack: fab
    link: https://www.instagram.com/bevsxyz
---

I am Bevan Stanely, an aspiring Data Scientist who loves storytelling with data. I was a biology research student in my previous life, fiddling with my computational model for bacterial behavior. The most exciting part of my research was putting together a story from my data. Well, that’s what I am pursuing as a career.

I blog here about programming, Linux, and hopefully the science stuff I think is cool(soon enough, I promise). While you are here, could I interest you in a few [projects](/#projects) of mine?

The best place to get to know me professionally is my [resume](/files/resume.pdf). [LinkedIn](https://in.linkedin.com/in/bevsxyz/
) does a fair job as well. Is there something I could help you with as a freelancer? Fill up the contact me form below, and I will get back to you.
