---
title: 'The Child Inside Me'
slug: child-inside-me
subtitle: 'I see a child....'
authors:
- admin
tags:
- Poem
categories:
- Poem
date: "2021-02-16T08:00:00+05:30"
---

A piece that came to me when I was alone at the break of the dawn. Depression is hard on its own; it is more challenging when people around you want help, but their support idea is not what you need.

When I look inside of me,

I see a child.

I can hear him crying. Wailing!

My heart yearns to console him,

to say it's alright,

this is just a phase...

Alas! all I feel is numbness,

eating me from the inside.
